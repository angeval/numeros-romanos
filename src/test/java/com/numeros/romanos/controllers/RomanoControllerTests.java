package com.numeros.romanos.controllers;

import com.numeros.romanos.services.RomanoService;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@WebMvcTest(NumeroRomanoController.class)
public class RomanoControllerTests {

    @MockBean
    RomanoService romanoService;

    @Autowired
    private MockMvc mockMvc;

    String numeroRomano;
    int numero;

    @Test
    public void testarConverterNumeroUm() throws Exception {
        numeroRomano = "I";
        numero = 1;

        Mockito.when(romanoService.converterRomanoParaNatural(Mockito.anyInt())).thenReturn(numeroRomano);

        mockMvc.perform(MockMvcRequestBuilders.get("/romanos/" + numero)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$"
                        , CoreMatchers.equalTo(numeroRomano)));
    }

    @Test
    public void testarConverterNumeroSimples() throws Exception {
        numeroRomano = "V";
        numero = 5;

        Mockito.when(romanoService.converterRomanoParaNatural(Mockito.anyInt())).thenReturn(numeroRomano);

        mockMvc.perform(MockMvcRequestBuilders.get("/romanos/" + numero)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$"
                        , CoreMatchers.equalTo(numeroRomano)));
    }

    @Test
    public void testarConverterNumeroDoisAlgarismos() throws Exception {
        numeroRomano = "LI";
        numero = 51;

        Mockito.when(romanoService.converterRomanoParaNatural(Mockito.anyInt())).thenReturn(numeroRomano);

        mockMvc.perform(MockMvcRequestBuilders.get("/romanos/" + numero)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$"
                        , CoreMatchers.equalTo(numeroRomano)));
    }

    @Test
    public void testarConverterNumeroAleatorio() throws Exception {
        numeroRomano = "LXIX";
        numero = 69;

        Mockito.when(romanoService.converterRomanoParaNatural(Mockito.anyInt())).thenReturn(numeroRomano);

        mockMvc.perform(MockMvcRequestBuilders.get("/romanos/" + numero)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$"
                        , CoreMatchers.equalTo(numeroRomano)));
    }

    @Test
    public void testarConverterNumeroTresAlgarismos() throws Exception {
        numeroRomano = "CXXIII";
        numero = 123;

        Mockito.when(romanoService.converterRomanoParaNatural(Mockito.anyInt())).thenReturn(numeroRomano);

        mockMvc.perform(MockMvcRequestBuilders.get("/romanos/" + numero)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$"
                        , CoreMatchers.equalTo(numeroRomano)));
    }

    @Test
    public void testarConverterNumeroQuatroAlgarismos() throws Exception {
        numeroRomano = "MDCXXXII";
        numero = 1632;

        Mockito.when(romanoService.converterRomanoParaNatural(Mockito.anyInt())).thenReturn(numeroRomano);

        mockMvc.perform(MockMvcRequestBuilders.get("/romanos/" + numero)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$"
                        , CoreMatchers.equalTo(numeroRomano)));
    }

    @Test
    public void testarConverterNumeroZero() throws Exception {
        numero = 0;

        Mockito.when(romanoService.converterRomanoParaNatural(Mockito.anyInt())).thenReturn(null);

        mockMvc.perform(MockMvcRequestBuilders.get("/romanos/" + numero)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testarConverterNumeroInvalido() throws Exception {
        Integer numero = null;

        Mockito.when(romanoService.converterRomanoParaNatural(Mockito.anyInt())).thenReturn(numeroRomano);

        mockMvc.perform(MockMvcRequestBuilders.get("/romanos/" + numero)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }
}

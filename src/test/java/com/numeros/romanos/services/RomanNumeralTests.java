package com.numeros.romanos.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class RomanNumeralTests {

    @Test
    void singleDigit() {
        Assertions.assertEquals(1, new RomanNumeral().romanToInt("I"));
        Assertions.assertEquals(5, new RomanNumeral().romanToInt("V"));
        Assertions.assertEquals(10, new RomanNumeral().romanToInt("X"));
        Assertions.assertEquals(50, new RomanNumeral().romanToInt("L"));
        Assertions.assertEquals(100, new RomanNumeral().romanToInt("C"));
        Assertions.assertEquals(500, new RomanNumeral().romanToInt("D"));
        Assertions.assertEquals(1000, new RomanNumeral().romanToInt("M"));
    }

    @Test
    void repetition() {
        Assertions.assertEquals(2, new RomanNumeral().romanToInt("II"));
        Assertions.assertEquals(20, new RomanNumeral().romanToInt("XX"));
    }

    @Test
    void manyLettersInOrder() {
        Assertions.assertEquals(6, new RomanNumeral().romanToInt("VI"));
        Assertions.assertEquals(15, new RomanNumeral().romanToInt("XV"));
    }

    @Test
    void subtractiveNotation() {
        Assertions.assertEquals(4, new RomanNumeral().romanToInt("IV"));
        Assertions.assertEquals(9, new RomanNumeral().romanToInt("IX"));
    }

    @Test
    void withAndWithoutSubtractiveNotation() {
        Assertions.assertEquals(14, new RomanNumeral().romanToInt("XIV"));
        Assertions.assertEquals(159, new RomanNumeral().romanToInt("CLIX"));
    }

    @Test
    void minAndMax() {
        Assertions.assertEquals(1, new RomanNumeral().romanToInt("I"));
        Assertions.assertEquals(3999, new RomanNumeral().romanToInt("MMMCMXCIX"));
    }

    @Test
    void invalidLetter() {
        Exception e = assertThrows(RuntimeException.class, () -> new RomanNumeral().romanToInt("Y"));

        Assertions.assertEquals("Invalid number", e.getMessage());
    }

    // TODO: We still miss testing for invalid numbers, such as 'IIIVII'.
    @Test
    void invalidSequence() {
        Exception e = assertThrows(RuntimeException.class, () -> new RomanNumeral().romanToInt("IIIVII"));

        Assertions.assertEquals("Invalid sequence", e.getMessage());
    }

}

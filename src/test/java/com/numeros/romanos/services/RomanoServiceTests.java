package com.numeros.romanos.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class RomanoServiceTests {

    @Test
    public void testarConverterNumeroUm(){
        int numero = 1;
        String resultado = "I";
        RomanoService romanoService = new RomanoService();
        Assertions.assertEquals(romanoService.converterRomanoParaNatural(numero), resultado);
    }

    @Test
    public void testarConverterNumeroSimples(){
        int numero = 5;
        String resultado = "V";
        RomanoService romanoService = new RomanoService();
        Assertions.assertEquals(romanoService.converterRomanoParaNatural(numero), resultado);
    }

    @Test
    public void testarConverterNumeroDoisAlgarismos(){
        int numero = 51;
        String resultado = "LI";
        RomanoService romanoService = new RomanoService();
        Assertions.assertEquals(romanoService.converterRomanoParaNatural(numero), resultado);
    }

    @Test
    public void testarConverterNumeroTresAlgarismos(){
        int numero = 132;
        String resultado = "CXXXII";
        RomanoService romanoService = new RomanoService();
        Assertions.assertEquals(romanoService.converterRomanoParaNatural(numero), resultado);
    }

    @Test
    public void testarConverterNumeroQuatroAlgarismos(){
        int numero = 1632;
        String resultado = "MDCXXXII";
        RomanoService romanoService = new RomanoService();
        Assertions.assertEquals(romanoService.converterRomanoParaNatural(numero), resultado);
    }

    @Test
    public void testarConverterNumeroAleatorio(){
        int numero = 69;
        String resultado = "LXIX";
        RomanoService romanoService = new RomanoService();
        Assertions.assertEquals(romanoService.converterRomanoParaNatural(numero), resultado);
    }

    @Test
    public void testarConverterNumeroZero(){
        int numero = 0;
        RomanoService romanoService = new RomanoService();
        Assertions.assertNull(romanoService.converterRomanoParaNatural(numero));
    }
    @Test
    public void testarConverterNumeroInvalido(){
        Integer numero = null;
        RomanoService romanoService = new RomanoService();
        Assertions.assertNull(romanoService.converterRomanoParaNatural(numero));
    }
}

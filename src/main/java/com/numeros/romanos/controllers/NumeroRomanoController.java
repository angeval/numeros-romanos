package com.numeros.romanos.controllers;

import com.numeros.romanos.services.RomanoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@RestController
@RequestMapping("/romanos")
public class NumeroRomanoController {

    @Autowired
    private RomanoService romanoService;

    @GetMapping("/{numero}")
    public String converterNumero(@PathVariable Integer numero){
        String numeroRomano = romanoService.converterRomanoParaNatural(numero);

        if(numeroRomano==null){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        else{
            return numeroRomano;
        }
    }
}

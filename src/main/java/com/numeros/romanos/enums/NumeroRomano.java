package com.numeros.romanos.enums;

public enum NumeroRomano {
    I(   1,"I"),
    IV(  4,"IV"),
    V(   5, "V"),
    IX(  9, "IX"),
    X(  10, "X"),
    XL( 40, "XL"),
    L(  50, "L"),
    XC( 90, "XC"),
    C( 100, "C"),
    CD(400, "CD"),
    D( 500, "D"),
    CM(900, "CM"),
    M(1000, "M");

    private int ponto;
    private String nome;

    NumeroRomano(int ponto, String nome) {
        this.ponto = ponto;
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }

    public int getPonto() {
        return ponto;
    }

}

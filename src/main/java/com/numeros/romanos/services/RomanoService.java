package com.numeros.romanos.services;

import com.numeros.romanos.enums.NumeroRomano;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class RomanoService {

    public String converterRomanoParaNatural(Integer numero) {
        //Verifica número inválido
        if(numero==null || numero ==0){
            return null;
        }

        //Verifica se existe, concatena o valor romano e subtrai do numero natural até zero
        List<NumeroRomano> lista = Arrays.asList(NumeroRomano.values());

        String valorRomano = "";
        int n = 0;

        do {
            n = numero;
            for (int i = lista.size() - 1; (i >= 0 && n > 0); i--) {
                if (lista.get(i).getPonto() == n) {
                    valorRomano = valorRomano + lista.get(i).getNome();
                    numero -= lista.get(i).getPonto();
                    n = 0;
                } else if (lista.get(i).getPonto() < n) {
                    valorRomano = valorRomano + lista.get(i).getNome();
                    numero -= lista.get(i).getPonto();
                    n = 0;
                }
            }
        } while (numero > 0);

        return valorRomano;
    }


}
